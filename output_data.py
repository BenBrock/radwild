import pickle
import numpy as np
import scipy.sparse
from scipy.io import mmwrite

X = pickle.load(open('news20.X.pickle', 'rb'))
y = pickle.load(open('news20.y.pickle', 'rb'))

y[y == -1] = 0

np.random.seed(0)

ridx = np.random.permutation(X.shape[0])
X = X[ridx]
y = y[ridx]

mmwrite('news20.X.mtx', X)
np.savetxt('news20.y', y)
