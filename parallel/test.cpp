#include <cstdlib>
#include <cstdio>
#include <string>
#include <vector>
#include <iostream>
#include <mpi.h>
#include <mkl.h>
#include "mat_lib.hpp"
#include "regression.hpp"

template <class T>
void print(std::vector <T> v, std::string lbl) {
  printf("%s:", lbl.c_str());
  for (auto i : v) {
    std::cout << " " << i;
  }
  printf("\n");
}

int main(int argc, char **argv) {
  MPI_Init(&argc, &argv);

  int rank, nprocs;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

  srand48(rank);

  /* Load dense y vector and sparse X matrix.
     Currently, a copy of these is held on each
     node. */

  std::string prefix = "/scratch/04682/tg839814/binary_datasets/";
  dense_vec_t *y = read_vec_binary(prefix + "kdda.noshuffle.y.bvecb");
  csr_mat_t *X = read_csr_binary(prefix + "kdda.noshuffle.X.bcsrb");

  int n_samples = X->m;
  float reg = 1.0f / n_samples;
  float stepsize_decay = 1.0f;

  int max_epochs = 20;

  int batch_size = 2048;
  int batches_per_sync = 1;
  float stepsize = 0.001;

  dense_vec_t *w = init_model(X->n, nprocs, rank);
  float *lw = allocate_local_model(X, nprocs);
 
  double begin, end;

  rcv_rq = rcv_fl = send_rq = send_fl = 0.0;

  MPI_Barrier(MPI_COMM_WORLD);

  MPI_Win_lock_all(0, *w->win);

  begin = dsecnd();

  float s = stepsize;
  for (int epoch = 0; epoch < max_epochs; epoch++) {
    do_epoch(X, y, w, lw, nprocs, rank, batch_size, batches_per_sync, reg, &s, stepsize_decay, 0.005);
    if (rank == 0) {
      end = dsecnd();
      float loss = get_loss(X, y, lw, reg);
      double comm = rcv_rq + rcv_fl + send_rq + send_fl;
      printf("t: %lf J: %f (rcv: %lf, %lf) (send: %lf, %lf) (comp: %lf) (comm: %lf) (%lf comm)\n", end - begin, loss, rcv_rq, rcv_fl, send_rq, send_fl, comp, comm, comm / (comm+comp));
      fflush(stdout);
    }
  }

  MPI_Win_unlock_all(*w->win);

  destroy_distr_dense_vec_t(w);
  free(lw);
/*

  static const int batch_sizes_tmp[] = {4096, 3072, 2048, 1024, 512, 256, 128, 64};
  static const int batches_per_syncs_tmp[] = {1};
  static const float stepsizes_tmp[] = {0.0001f, 0.0005f, 0.001f, 0.005f, 0.01f, 0.05f, 0.1f, 0.5f, 0.1f};

  std::vector <int> batch_sizes (batch_sizes_tmp, batch_sizes_tmp + sizeof(batch_sizes_tmp) / sizeof(batch_sizes_tmp[0]));
  std::vector <int> batches_per_syncs (batches_per_syncs_tmp, batches_per_syncs_tmp + sizeof(batches_per_syncs_tmp) / sizeof(batches_per_syncs_tmp[0]));
  std::vector <float> stepsizes (stepsizes_tmp, stepsizes_tmp + sizeof(stepsizes_tmp) / sizeof(stepsizes_tmp[0]));

  if (rank == 0) {
    print(batch_sizes, "batch_sizes");
    print(batches_per_syncs, "batches_per_sync");
    print(stepsizes, "stepsizes");
  }

  for (int batch_size : batch_sizes) {
    for (int batches_per_sync : batches_per_syncs) {
      for (float stepsize : stepsizes) {

        dense_vec_t *w = init_model(X->n, nprocs, rank);
        float *lw = allocate_local_model(X, nprocs);

        double begin, end;
        begin = dsecnd();
        end = dsecnd();

        MPI_Barrier(MPI_COMM_WORLD);

        begin = dsecnd();

        MPI_Win_lock_all(0, *w->win);

        float s = stepsize;

        for (int epoch = 0; epoch < max_epochs; epoch++) {
          printf("Begin epoch... %d\n", rank);
          fflush(stdout);
          do_epoch(X, y, w, lw, nprocs, rank, batch_size, batches_per_sync, reg, &s, stepsize_decay, 0.001);
          printf("Finished epoch... %d\n", rank);
          fflush(stdout);
          if (rank == 0) {
            end = dsecnd();
            printf("Calculating loss... %d\n", rank);
            fflush(stdout);
            float loss = get_loss(X, y, lw, reg);
            printf("Finished calculating loss... %d\n", rank);
            fflush(stdout);
            printf("t: %f bps: %d bs: %d sd: %f s: %f J: %f\n", end - begin, batches_per_sync, batch_size, stepsize_decay, stepsize, loss);
          }
        }

        MPI_Win_unlock_all(*w->win);

        destroy_distr_dense_vec_t(w);
        free(lw);
      }
    }
  }
*/

  destroy_local_dense_vec_t(y);
  destroy_csr_mat_t(X);

  MPI_Finalize();

  return 0;
}
