
#pragma once

#include <cstdlib>
#include <cstdio>
#include <mpi.h>
#include <mkl.h>
#include <math.h>
#include "mat_lib.hpp"

double comp;
double rcv_fl, rcv_rq;
double send_fl, send_rq;

dense_vec_t *init_model(int d, int nprocs, int rank) {
  int split = (d + nprocs - 1) / nprocs;
  int my_start = split * rank;
  int my_size = std::min(split, d - my_start);

  dense_vec_t *model = (dense_vec_t *) malloc(sizeof(dense_vec_t));
  model->n = my_size;
  model->win = (MPI_Win *) malloc(sizeof(MPI_Win));
  model->info = (MPI_Info *) malloc(sizeof(MPI_Info));

  MPI_Info_create(model->info);
  MPI_Info_set(*model->info, "accumulate_ordering", "none");
  MPI_Info_set(*model->info, "accumulate_ops", "same_op_no_op");
  MPI_Info_set(*model->info, "same_size", "true");
  MPI_Info_set(*model->info, "same_disp_unit", "true");

  MPI_Win_allocate(split*sizeof(int), sizeof(int), *model->info, MPI_COMM_WORLD, &model->v, model->win);

  // MPI_Alloc_mem(split*sizeof(float), MPI_INFO_NULL, &model->v);

  for (int i = 0; i < split; i++) {
    model->v[i] = 0.0f;
  }

  // MPI_Win_create(model->v, split*sizeof(float), sizeof(float), MPI_INFO_NULL, MPI_COMM_WORLD, model->win);

  return model;
}

float *allocate_local_model(csr_mat_t *X, int nprocs) {
  int d = X->n;
  int split = (d + nprocs - 1) / nprocs;

  float *lw = (float *) malloc(sizeof(float) * split*nprocs);
  for (int i = 0; i < split*nprocs; i++) {
    lw[i] = 0.0f;
  }
  return lw;
}

void flush(MPI_Request *requests, int n) {
  for (int i = 0; i < n; i++) {
    MPI_Wait(&requests[i], MPI_STATUS_IGNORE);
  }
}

void retrieve_local_model(dense_vec_t *w, csr_mat_t *X, int nprocs, float *lw) {
  int d = X->n;
  int split = (d + nprocs - 1) / nprocs;

  MPI_Request *requests = (MPI_Request *) malloc(sizeof(MPI_Request) * nprocs);

  double begin = dsecnd();
  for (int i = 0; i < nprocs; i++) {
    MPI_Rget_accumulate(NULL, split, MPI_FLOAT, lw + (i*split), split, MPI_FLOAT, i, 0, split, MPI_FLOAT, MPI_NO_OP, *w->win, &requests[i]);
  }
  double end = dsecnd();
  rcv_rq += end - begin;
  begin = dsecnd();
  flush(requests, nprocs);
  end = dsecnd();
  rcv_fl += end - begin;
  free(requests);
}

void store_global_model(dense_vec_t *w, csr_mat_t *X, int nprocs, float *lw) {
  int d = X->n;
  int split = (d + nprocs - 1) / nprocs;

  MPI_Request *requests = (MPI_Request *) malloc(sizeof(MPI_Request) * nprocs);
  double begin = dsecnd();
  for (int i = 0; i < nprocs; i++) {
    MPI_Raccumulate(lw+(i*split), split, MPI_FLOAT, i, 0, split, MPI_FLOAT, MPI_SUM, *w->win, &requests[i]);
  }
  double end = dsecnd();
  send_rq += end - begin;
  begin = dsecnd();
  flush(requests, nprocs);
  end = dsecnd();
  send_fl += end - begin;
  free(requests);
}

void do_batch(csr_mat_t *X, dense_vec_t *y, dense_vec_t *w, float *lw, int nprocs,
              int rank, int batch_size, float reg, float stepsize, int batch,
              float *update) {
  /* n, d */
  int n_samples = X->m;
  int d = X->n;
  int split = (d + nprocs - 1) / nprocs;

  int my_start = batch*batch_size;
  int my_size = std::min(batch_size, n_samples - my_start);

  /* Get sub-matrix that you're going to be using for gradient. */
  int m = my_size;
  int n = X->n;

  int *row_ptr = X->row_ptr+my_start;
  float *vals = X->vals+*row_ptr;
  int *col_ind = X->col_ind+*row_ptr;

  /* Portion of y to be used. */
  float *ly = y->v+my_start;

  float *inter = (float *) malloc(sizeof(float) * my_size);

  char N = 'N';
  char T = 'T';

  char matdescra[6] = {'G', 'L', 'N', 'C', 'X', 'X'};

  float one = 1.0f;
  float zero = 0.0f;

  mkl_scsrmv(&N, &m, &n, &one, matdescra, vals, col_ind, row_ptr, row_ptr+1, lw, &zero, inter);
 
  /* Should optimize this. */
  for (int i = 0; i < my_size; i++) {
    inter[i] = ly[i] - (1.0f / (1.0f + expf(fminf(-inter[i], 80.0f))));
  }

  float *l_update = (float *) malloc(sizeof(float) * split*nprocs);

  mkl_scsrmv(&T, &m, &n, &one, matdescra, vals, col_ind, row_ptr, row_ptr+1, inter, &zero, l_update);

  for (int i = 0; i < d; i++) {
    update[i] += stepsize*(l_update[i] - 2.0f*reg*lw[i]);
    lw[i] += stepsize*(l_update[i] - 2.0f*reg*lw[i]);
  }

  free(l_update);
  free(inter);
}

void do_epoch(csr_mat_t *X, dense_vec_t *y, dense_vec_t *w, float *lw, int nprocs,
              int rank, int batch_size, int batches_per_sync, float reg, float *stepsize,
              float stepsize_decay, double discount) {
  /* n, Number of data samples */
  int n_samples = X->m;
  int d = X->n;
  int split = (d + nprocs - 1) / nprocs;


  float sd = powf(stepsize_decay, 1.0f / ((n_samples + batch_size - 1) / batch_size));

  int n_batches = ((int) (discount*n_samples) + batch_size*batches_per_sync - 1) / (batch_size*batches_per_sync);
  int batches = (n_samples + batch_size - 1) / (batch_size);
  // int start_batch = 0;
  int start_batch = lrand48() % batches;

  float *update = (float *) malloc(sizeof(float) * split*nprocs);
  for (int i = 0; i < n_batches; i++) {
    double begin = dsecnd();
    for (int j = 0; j < split*nprocs; j++) {
      update[j] = 0.0f;
    }
    double end = dsecnd();
    comp += end - begin;
    int batch = ((i*batches_per_sync) + start_batch) % batches; 
    retrieve_local_model(w, X, nprocs, lw);

    begin = dsecnd();
    for (int j = 0; j < batches_per_sync; j++) {
      do_batch(X, y, w, lw, nprocs, rank, batch_size, reg, *stepsize, batch, update);
      batch = (batch + 1) % batches;
      *stepsize = sd * (*stepsize);
    }
    end = dsecnd();
    comp += end - begin;

    /* Store global model. */
    store_global_model(w, X, nprocs, update);
  }

  free(update);
}

float get_loss(csr_mat_t *X, dense_vec_t *y, float *lw, float reg) {
  /* Get sub-matrix that you're going to be using for gradient. */
  int m = X->m;
  int n = X->n;

  float *ly = y->v;
  float *vals = X->vals;
  int *col_ind = X->col_ind;
  int *row_ptr = X->row_ptr;

  float *inter = (float *) malloc(sizeof(float) * m);

  char N = 'N';
  char T = 'T';

  char matdescra[6] = {'G', 'L', 'N', 'C', 'X', 'X'};

  float one = 1.0f;
  float zero = 0.0f;

  mkl_scsrmv(&N, &m, &n, &one, matdescra, vals, col_ind, row_ptr, row_ptr+1, lw, &zero, inter);

  for (int i = 0; i < m; i++) {
    inter[i] = (1.0f / (1.0f + expf(-inter[i])));
  }

  float sum = 0.0f;

  for (int i = 0; i < m; i++) {
    sum += ly[i]*logf(fmaxf(inter[i], 1e-20f)) + (1.0f - ly[i])*logf(fmaxf(1.0f - inter[i], 1e-20f));
  }

  float wsum = 0.0f;

  for (int i = 0; i < n; i++) {
    wsum += lw[i]*lw[i];
  }

  free(inter);

  return -sum + reg*wsum;
}
