
#pragma once

#include <fstream>
#include <cstdlib>
#include <cstdio>
#include <map>
#include <regex.h>
#include <mpi.h>

struct dense_vec_t {
  float *v;
  int n;

  MPI_Win *win;
  MPI_Info *info;
};

struct csr_mat_t {
  int nnz;
  int m, n;
  int *row_ptr;
  int *col_ind;
  float *vals;
};

dense_vec_t *read_vec(std::string fname) {
  std::string buf;
  std::ifstream f;

  f.open(fname.c_str());

  if (!f.is_open()) {
    fprintf(stderr, "Error: cannot open %s.\n", fname.c_str());
    exit(0);
  }

  int file_size = 0;
  while (getline(f, buf)) {
    file_size++;
  }
  f.close();

  dense_vec_t *v = (dense_vec_t *) malloc(sizeof(dense_vec_t));
  v->n = file_size;
  v->v = (float *) malloc(sizeof(float) * v->n);

  f.open(fname.c_str());

  int i = 0;
  while (getline(f, buf)) {
    v->v[i] = atof(buf.c_str());
    i++;
  }
  f.close();

  return v;
}

csr_mat_t *read_sorted_csr(std::string fname) {
  std::ifstream f;

  f.open(fname.c_str());

  if (!f.is_open()) {
    fprintf(stderr, "Error: cannot open %s.\n", fname.c_str());
    exit(1);
  }

  std::string buf;

  bool outOfComments = false;

  while (!outOfComments) {
    getline(f, buf);
    regex_t regex;
    int reti;

    reti = regcomp(&regex, "^%", 0);
    reti = regexec(&regex, buf.c_str(), 0, NULL, 0);

    if (reti == REG_NOMATCH) {
      outOfComments = true;
    }
  }

  int m, n, nnz;

  sscanf(buf.c_str(), "%d %d %d", &m, &n, &nnz);

  csr_mat_t *mat = (csr_mat_t *) malloc(sizeof(csr_mat_t));
  mat->nnz = nnz;
  mat->m = m;
  mat->n = n;

  mat->vals = (float *) malloc(sizeof(float) * mat->nnz);
  mat->row_ptr = (int *) malloc(sizeof(int) * (mat->m+1));
  mat->col_ind = (int *) malloc(sizeof(int) * mat->nnz);

  mat->row_ptr[0] = 0;
  int r = 0;
  int c = 0;
  int i0 = 0;
  int j0 = 0;
  while (getline(f, buf)) {
    int i, j;
    float v;
    sscanf(buf.c_str(), "%d %d %f", &i, &j, &v);
    i--;
    j--;

    if ((i == i0 && j < j0) || i < i0) {
      fprintf(stderr, "Error: matrix \"%s\" is not sorted.\n", fname.c_str());
      exit(0);
    }
    i0 = i;
    j0 = j;

    mat->vals[c] = v;
    mat->col_ind[c] = j;

    while (r < i) {
      mat->row_ptr[r+1] = c;
      r++;
    }
    c++;
  }
  mat->row_ptr[mat->m] = mat->nnz;

  f.close();

  return mat;
}

csr_mat_t *read_csr(std::string fname) {
  std::ifstream f;

  f.open(fname.c_str());

  if (!f.is_open()) {
    fprintf(stderr, "Error: cannot open %s.\n", fname.c_str());
    exit(1);
  }

  std::string buf;

  bool outOfComments = false;

  while (!outOfComments) {
    getline(f, buf);
    regex_t regex;
    int reti;

    reti = regcomp(&regex, "^%", 0);
    reti = regexec(&regex, buf.c_str(), 0, NULL, 0);

    if (reti == REG_NOMATCH) {
      outOfComments = true;
    }
  }

  int m, n, nnz;

  sscanf(buf.c_str(), "%d %d %d", &m, &n, &nnz);

  csr_mat_t *mat = (csr_mat_t *) malloc(sizeof(csr_mat_t));
  mat->nnz = nnz;
  mat->m = m;
  mat->n = n;

  mat->vals = (float *) malloc(sizeof(float) * mat->nnz);
  mat->row_ptr = (int *) malloc(sizeof(int) * (mat->m+1));
  mat->col_ind = (int *) malloc(sizeof(int) * mat->nnz);

  std::map <int, std::map <int, float>> entries;

  for (int i = 0; i < m; i++) {
    entries[i] = std::map <int, float>();
  }

  while (getline(f, buf)) {
    int i, j;
    float v;
    sscanf(buf.c_str(), "%d %d %f", &i, &j, &v);
    i--;
    j--;

    entries[i][j] = v;
  }

  int i = 0;
  int c = 0;
  mat->row_ptr[0] = 0;
  for (auto row : entries) {
    mat->row_ptr[i+1] = mat->row_ptr[i] + row.second.size();
    for (auto entry : row.second) {
      mat->col_ind[c] = entry.first;
      mat->vals[c] = entry.second;
      c++;
    }
    i++;
  }

  f.close();

  return mat;
}

void write_vec_binary(std::string fname, dense_vec_t *y) {
  FILE *f = fopen(fname.c_str(), "w");

  if (f == NULL) {
    fprintf(stderr, "Error: couldn't open \"%s\"\n", fname.c_str());
    exit(1);
  }

  fwrite(&y->n, sizeof(int), 1, f);

  fwrite(y->v, sizeof(float), y->n, f);

  fclose(f);
}

dense_vec_t *read_vec_binary(std::string fname) {
  FILE *f = fopen(fname.c_str(), "r");

  if (f == NULL) {
    fprintf(stderr, "Error: couldn't open \"%s\"\n", fname.c_str());
    exit(1);
  }


  dense_vec_t *y = (dense_vec_t *) malloc(sizeof(dense_vec_t));

  fread(&y->n, sizeof(int), 1, f);

  y->v = (float *) malloc(sizeof(float) * y->n);

  fread(y->v, sizeof(float), y->n, f);

  fclose(f);

  return y;
}

void write_csr_binary(std::string fname, csr_mat_t *X) {
  FILE *f = fopen(fname.c_str(), "w");

  if (f == NULL) {
    fprintf(stderr, "Error: couldn't open \"%s\"\n", fname.c_str());
    exit(1);
  }

  fwrite(&X->nnz, sizeof(int), 1, f);
  fwrite(&X->m, sizeof(int), 1, f);
  fwrite(&X->n, sizeof(int), 1, f);

  fwrite(X->vals, sizeof(float), X->nnz, f);
  fwrite(X->col_ind, sizeof(int), X->nnz, f);
  fwrite(X->row_ptr, sizeof(int), X->m+1, f);
  fclose(f);
}

csr_mat_t *read_csr_binary(std::string fname) {
  FILE *f = fopen(fname.c_str(), "r");

  if (f == NULL) {
    fprintf(stderr, "Error: couldn't open \"%s\"\n", fname.c_str());
    exit(1);
  }

  csr_mat_t *X = (csr_mat_t *) malloc(sizeof(csr_mat_t));

  fread(&X->nnz, sizeof(int), 1, f);
  fread(&X->m, sizeof(int), 1, f);
  fread(&X->n, sizeof(int), 1, f);

  X->vals = (float *) malloc(sizeof(float) * X->nnz);
  X->col_ind = (int *) malloc(sizeof(int) * X->nnz);
  X->row_ptr = (int *) malloc(sizeof(int) * (X->m+1));

  fread(X->vals, sizeof(float), X->nnz, f);
  fread(X->col_ind, sizeof(int), X->nnz, f);
  fread(X->row_ptr, sizeof(int), X->m+1, f);
  fclose(f);

  return X;
}

void destroy_local_dense_vec_t(dense_vec_t *v) {
  free(v->v);
  free(v);
}

void destroy_distr_dense_vec_t(dense_vec_t *v) {
  MPI_Win_free(v->win);
  free(v->win);
  MPI_Info_free(v->info);
  free(v->info);
  free(v);
}

void destroy_csr_mat_t(csr_mat_t *m) {
  free(m->vals);
  free(m->col_ind);
  free(m->row_ptr);
}
