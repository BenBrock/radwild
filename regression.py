import pickle
import numpy as np
import scipy.sparse
from scipy.optimize import minimize
import sys

def vlog(x):
    return np.float64(1) / (np.float64(1) + np.exp(-x))

def epoch(X, y, w, b, stepsize, reg):
    i = 0
    while i < X.shape[0]:
        bs = min(b, X.shape[0] - i)
        x = X[i:i+bs,:]
        yi = y[i:i+bs]
        gradient = x.transpose().dot(yi - vlog(x.dot(w))) - 2*reg*w
        w += stepsize*gradient
        i += b
    return w

def loss(X, y, w, reg):
    # Same as np.sum(np.log(np.exp((1 - y)*(X.dot(w))) + np.exp(-y*(X.dot(w))))) + reg*(np.linalg.norm(w)**2)
    return -np.sum(y.dot(np.log(vlog(X.dot(w)))) + (1 - y).dot(np.log(np.abs(1 - vlog(X.dot(w)))))) + reg*(np.linalg.norm(w)**2)

X = pickle.load(open('news20.X.pickle', 'rb'))
y = pickle.load(open('news20.y.pickle', 'rb'))

reg = np.float64(1) / np.float64(X.shape[0])

np.random.seed(0)

ridx = np.random.permutation(X.shape[0])
X = X[ridx]
y = y[ridx]

y[y == -1] = 0

w = np.zeros(X.shape[1])

X = X.astype(np.float32)
y = y.astype(np.float32)
w = w.astype(np.float32)

epochs = 40
oldLoss = 0

stepsize = 0.9
batch_size = 128

for e in range(epochs):
    l = loss(X, y, w, reg)
    print('%s: %s (%s)' % (e, l, l - oldLoss))
    w = epoch(X, y, w, batch_size, stepsize, reg)
    oldLoss = l
