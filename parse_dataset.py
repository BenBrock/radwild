import re
import numpy as np
import scipy.sparse
import sys
import pickle
import time

def load_dataset(fname, feature_size):
    begin = time.time()
    num_lines = sum(1 for line in open('news20.binary'))

    X = scipy.sparse.lil_matrix((num_lines, feature_size))
    y = np.zeros(num_lines)

    i = 0
    for line in open('news20.binary', 'r').readlines():
        items = line.split(' ')

        label = items[0]
        features = items[1:]

        for feature in features:
            if feature.rstrip() == '':
                continue
            m = re.match('^(\d+)?:(.+)$', feature.rstrip())
            if not m:
                print('\"%s\"' % (feature.rstrip()))
            assert(m)
            idx = int(m[1])
            val = np.float64(m[2])
            X[i, idx-1] = val

        y[i] = label

        i += 1

    X = X.tocsr()

    end = time.time()

    print('Parsed \'%s\' in %s' % (fname, end - begin))

    return (X,y)

X,y = load_dataset('news20.binary', 1355191)

pickle.dump(X, open('news20.X.pickle', 'wb'))
pickle.dump(y, open('news20.y.pickle', 'wb'))
