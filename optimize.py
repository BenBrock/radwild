import pickle
import numpy as np
import scipy.sparse
from scipy.optimize import minimize

def vlog(x):
    return np.float64(1) / (np.float64(1) + np.exp(-x))

X = pickle.load(open('news20.X.pickle', 'rb'))
y = pickle.load(open('news20.y.pickle', 'rb'))

reg = np.float64(1) / X.shape[0]

ridx = np.random.permutation(X.shape[0])
X = X[ridx]
y = y[ridx]

y[y == -1] = 0

w = np.zeros(X.shape[1])

fn = lambda w: -np.sum(y.dot(np.log(vlog(X.dot(w)))) + (1 - y).dot(np.log(1 - vlog(X.dot(w))))) + reg*(np.linalg.norm(w)**2)
grad = lambda w: 2*reg*w - X.transpose().dot(y - vlog(X.dot(w)))

optimal_w = minimize(fn, w, method='L-BFGS-B', jac=grad)
print(optimal_w)
